package com.br.beautypass.model;

import org.json.simple.JSONArray;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;

public class ScheduledService {


    private int id = -1,
            idStore = -1,
            idService = -1,
            idUser = -1,
            score  = -1,
            confirmed = -1;
    private String nameService,
            dateString,
            statusStr;
    private double value = -1;
    private Store store;
    private User client;
    private boolean executed;
    private ArrayList<ServiceOption> listOfOptions;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setIdService(int idService) {
        this.idService = idService;
    }
    public int getScore() {
        return score;
    }
    public void setScore(int score) {
        this.score = score;
    }
    public void setNameService(String nameService) {
        this.nameService = nameService;
    }
    public double getValue() {
        return value;
    }
    public void setValue(double value) {
        this.value = value;
    }
    public int getConfirmed() {
        return confirmed;
    }
    public void setConfirmed(int confirmed) {
        this.confirmed = confirmed;
    }
    public Store getStore() {
        return store;
    }
    public void setStore(Store store) {
        this.store = store;
    }
    public boolean isExecuted() {
        return executed;
    }
    public void setExecuted(boolean executed) {
        this.executed = executed;
    }
    public User getClient() {
        return client;
    }
    public void setClient(User client) {
        this.client = client;
    }

    public ArrayList<ServiceOption> getListOfOptions() {
        return listOfOptions;
    }

    public void setListOfOptions(ArrayList<ServiceOption> listOfOptions) {
        this.listOfOptions = listOfOptions;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }
}
