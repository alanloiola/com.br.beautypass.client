package com.br.beautypass.model;

import java.util.ArrayList;

public class DateService {

    private String date;
    private String dayOfWeek;
    private ArrayList<TimeService> listOfTime;
    private TimeService timeSelected;

    public String getDate() {
        return date + ", " +dayOfWeek;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public ArrayList<TimeService> getListOfTime() {
        return listOfTime;
    }

    public void setListOfTime(ArrayList<TimeService> listOfTime) {
        this.listOfTime = listOfTime;
    }

    public TimeService getTimeSelected() {
        return timeSelected;
    }

    public void setTimeSelected(TimeService timeSelected) {
        this.timeSelected = timeSelected;
    }

    public void setTimeSelected(int position) {
        if (position < listOfTime.size()){
            setTimeSelected(listOfTime.get(position));
        }
    }

    @Override
    public String toString() {
        return getDate();
    }

    public ArrayList<String> getTimeString(){
        ArrayList<String> listOfString = new ArrayList<>();
        for (TimeService time : listOfTime){
            listOfString.add(time.getTime());
        }
        return listOfString;
    }
}
