package com.br.beautypass.model;

import java.util.ArrayList;
import java.util.List;

public class HomeConfiguration {

    private ArrayList<Store> listOfStores;
    private ArrayList<Category> listOfCategorys;
    private int limitStoresHome = -1,
                limitCategorysHome = -1;

    public ArrayList<Store> getListOfStores() {
        return listOfStores;
    }

    public void setListOfStores(ArrayList<Store> listOfStores) {
        this.listOfStores = listOfStores;
    }

    public ArrayList<Category> getListOfCategorys() {
        return listOfCategorys;
    }

    public void setListOfCategorys(ArrayList<Category> listOfCategorys) {
        this.listOfCategorys = listOfCategorys;
    }

    public boolean isEmpty(){
        return listOfStores == null || listOfStores.isEmpty();
    }

    public List<Category> getListOfCategorysHome() {
        if (limitCategorysHome == -1){
            return listOfCategorys;
        }else{
            return listOfCategorys.subList(0, limitCategorysHome);
        }
    }

    public List<Store> getListOfStoresHome() {
        if (limitStoresHome == -1 || limitStoresHome > listOfStores.size()){
            return listOfStores;
        }else{
            return listOfStores.subList(0, limitStoresHome);
        }
    }

    public ArrayList<Store> getListOfStoresFiltredByType(int idType) {
        ArrayList<Store> listFiltred = new ArrayList<>();
        for (Store store : listOfStores){
            if (store.getCategory().getId() == idType)
                listFiltred.add(store);
        }
        return listFiltred;
    }

    public void setLimitStoresHome(int limitStoresHome) {
        this.limitStoresHome = limitStoresHome;
    }

    public void setLimitCategorysHome(int limitCategorysHome) {
        this.limitCategorysHome = limitCategorysHome;
    }
}
