package com.br.beautypass.model;

public class TimeService {

    private String time;
    private Long timeDate;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Long getTimeDate() {
        return timeDate;
    }

    public void setTimeDate(Long timeDate) {
        this.timeDate = timeDate;
    }
}
