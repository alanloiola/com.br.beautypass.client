package com.br.beautypass.client.fragments.search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.Nullable;

import com.br.beautypass.client.R;
import com.br.beautypass.client.adapter.ListResultStoresAdapter;
import com.br.beautypass.client.fragments.details.Details_Element;

public class List_result_search_fragment extends Details_Element {

    private Search_by_title lastPage;

    public List_result_search_fragment(Search_by_title lastPage) {
        this.lastPage = lastPage;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_list_result_search, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        final ListView listViewResult = (ListView) view.findViewById(R.id.resultSearch);

        //Altera a altura da lista
        //ViewGroup.LayoutParams params = listViewResult.getLayoutParams();
        //params.height = 180 * lastPage.getSearchConfiguration().getResultStores().size();
       // listViewResult.setLayoutParams(params);
        listViewResult.setScrollContainer(false);

        ListResultStoresAdapter listAdapter = new ListResultStoresAdapter(lastPage.getSearchConfiguration().getResultStores(), lastPage);
        listViewResult.setAdapter(listAdapter);

    }
}
