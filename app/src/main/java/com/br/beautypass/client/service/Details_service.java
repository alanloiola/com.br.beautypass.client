package com.br.beautypass.client.service;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.beautypass.client.R;
import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.client.fragments.details.services.Confirm_service_selected;
import com.br.beautypass.client.fragments.details.services.Date_select_service;
import com.br.beautypass.client.fragments.details.services.List_options_services;
import com.br.beautypass.client.fragments.infoFragment.Error_fragment;
import com.br.beautypass.client.fragments.infoFragment.Loading_fragment;
import com.br.beautypass.manager.DateStoreManager;
import com.br.beautypass.manager.MarkServiceManager;
import com.br.beautypass.manager.UserExperience;
import com.br.beautypass.model.DateService;
import com.br.beautypass.model.GoogleCalendarEvent;
import com.br.beautypass.model.ScheduledService;
import com.br.beautypass.model.Service;
import com.br.beautypass.model.ServiceOption;
import com.br.beautypass.model.Store;
import com.br.beautypass.model.TimeService;
import com.br.beautypass.util.CalcUtils;
import com.br.beautypass.util.InterfaceUtils;

import java.util.ArrayList;

public class Details_service extends AppCompatActivity {

    private Button submit;
    private Store store;
    private Service service;
    private TextView finalValue;
    private int step = 0;
    private List_options_services listOptions;

    private boolean loading = false, error = false;
    private DateService dateSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_service);

        TextView title = (TextView) findViewById(R.id.title);
        TextView description = (TextView) findViewById(R.id.description);
        finalValue = (TextView) findViewById(R.id.final_value);

        submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextStep();
            }
        });

        ImageView backButton = (ImageView) findViewById(R.id.back);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //Recebe as configurações
        Intent intentRecebida = getIntent();
        if (intentRecebida!=null) {
            Bundle bundle = intentRecebida.getExtras();
            if (bundle != null) {

                Object store = bundle.get("store");
                Object service = bundle.get("service");

                if (store != null && service != null){

                    this.store = (Store)store;
                    this.service = (Service)service;

                    title.setText(this.service.getTitle());
                    if (this.service.getDescription() != null){
                        description.setText(this.service.getDescription());
                    }else{
                        description.setText("");
                    }

                    UserExperience.enterService(this.service, this.store);
                    goToServiceOptions();

                }else{
                    title.setText("Erro ao obter os dados");
                    description.setText("");
                }

            }
        }

    }

    public void nextStep(){
        if (!loading && !error) {
            switch (step) {
                case 0:
                    if (listOptions.getListAdapter() != null &&
                            listOptions.getListAdapter().hasSomeoneMarked()) {
                        goToDateSelect();
                    } else {
                        showMessageError("Selecione ao menos um item");
                    }
                    break;
                case 1:
                    finalValue.setVisibility(View.INVISIBLE);
                    submit.setText(R.string.button_confirm_service);
                    goToConfirm();
                    break;
                case 2:
                    showConfirmMessage();
                    break;
            }
        }else if (loading){
            showMessageError("Espere a finalização da operação");
        }else if (error){
            showMessageError("Houve um erro");
        }
    }

    public void showConfirmMessage(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Confirmação");
        builder.setMessage("Você confirma o agendamento para o dia "+getDateSelected().getDate()+" às " +
                getDateSelected().getTimeSelected().getTime()+" em "+store.getTitle()+"?");
        builder.setPositiveButton("Confirmar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finalizeService();
                    }
                });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void finalizeService(){
        setLoadingFragment();
        MarkServiceManager.getInstance().sendServiceMarker(service.getId(), dateSelected.getTimeSelected().getTimeDate(),
                getTotalValue(), getOptionsSelected(), this);
    }

    public void callBackFinalizeSucessService(){
        UserExperience.finalizeService(this.service);
        this.finish();
        Intent sendIntent = new Intent(Details_service.this, Default_Message.class);
        Bundle bundleInf= new Bundle();
        bundleInf.putSerializable("error", false);
        bundleInf.putSerializable("calendarObj", getGoogleCalendarEvent());
        bundleInf.putSerializable("msg", "Sucesso ao  marcar o serviço!");
        sendIntent.putExtras(bundleInf);
        startActivity(sendIntent);
    }

    public void callBackFinalizeErrorService(String message){
        this.finish();
        Intent sendIntent = new Intent(Details_service.this, Default_Message.class);
        Bundle bundleInf= new Bundle();
        bundleInf.putSerializable("error", true);
        bundleInf.putSerializable("msg", message);
        sendIntent.putExtras(bundleInf);
        startActivity(sendIntent);
    }

    private GoogleCalendarEvent getGoogleCalendarEvent(){
        GoogleCalendarEvent googleCalendarEvent = new GoogleCalendarEvent();
        googleCalendarEvent.setTexts(service, store);
        googleCalendarEvent.setTime(dateSelected.getTimeSelected().getTimeDate(), service.getDuration());
        return googleCalendarEvent;
    }

    public void showMessageError(String error) {
        InterfaceUtils.showMensage(error, this.getApplicationContext());
    }

    public void updateFinalValue(){
        finalValue.setText("Total: "+ CalcUtils.getFormatedValueMoney(getTotalValue()));
    }

    public void changeFragment(Fragment_menu new_fragment){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameDetails, new_fragment.getFragment()) // replace flContainer
                .addToBackStack(null)
                .commit();
    }


    public void goToDateSelect(){
        step = 1;
        setLoadingFragment();
        DateStoreManager.getInstance().processServiceDate(service.getId(), this);
    }

    private void setLoadingFragment(){
        loading = true;
        error = false;
        Loading_fragment loading_fragment = new Loading_fragment();
        changeFragment(loading_fragment);
    }

    public void goToServiceOptions(){
        step = 0;
        listOptions = new List_options_services(this);
        changeFragment(listOptions);
    }

    public void goToConfirm(){
        if (getDateSelected() != null && getDateSelected().getTimeSelected() != null){
            step = 2;
            Confirm_service_selected confirmFragment = new Confirm_service_selected(this);
            changeFragment(confirmFragment);
        }else{
            InterfaceUtils.showMensage("Erro ao selecionar as datas", this);
        }
    }

    public void callBackDateSelect(ArrayList<DateService> listOfDate){
        loading = false;
        Date_select_service dateFragment = new Date_select_service(this, listOfDate);
        changeFragment(dateFragment);
    }

    public void callBackErrorDateSelect(String message){
        error = true;
        Error_fragment error_fragment = new Error_fragment(message);
        changeFragment(error_fragment);
    }

    @Override
    public void onBackPressed() {
        if (!loading) {
            switch (step) {
                case 0:
                    this.finish();
                    break;
                case 1:
                    goToServiceOptions();
                    break;
                case 2:
                    finalValue.setVisibility(View.VISIBLE);
                    submit.setText(R.string.button_select_service);
                    goToDateSelect();
                    break;
            }
        }else{
            showMessageError("Aguarde a finalização da operação");
        }
    }

    public Store getStore() {
        return store;
    }

    public Service getService() {
        return service;
    }

    public ArrayList<ServiceOption> getOptionsSelected(){
        ArrayList<ServiceOption> listOfSelectedItens = new ArrayList<>();
        for (ServiceOption serviceOption : service.getServiceOption()){
            if (serviceOption.isChecked())
                listOfSelectedItens.add(serviceOption);
        }
        return listOfSelectedItens;
    }

    public Double getTotalValue(){
        double val = 0;
        for (ServiceOption serviceOption : service.getServiceOption()){
            if (serviceOption.isChecked())
                val += serviceOption.getValor();
        }
        return val;
    }

    public DateService getDateSelected() {
        return dateSelected;
    }

    public TimeService getTimeSelected(){
        return dateSelected.getTimeSelected();
    }

    public void setTimeSelected(int timePosition){
        dateSelected.setTimeSelected(timePosition);
    }

    public void setDateSelected(DateService dateSelected) {
        this.dateSelected = dateSelected;
    }
}
