package com.br.beautypass.client.fragments.menu;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.br.beautypass.client.R;
import com.br.beautypass.client.adapter.ListOptionsAdapter;
import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.client.fragments.options.Option_fragment;
import com.br.beautypass.manager.OptionsManager;
import com.br.beautypass.manager.UserManager;
import com.br.beautypass.util.InterfaceUtils;

import java.util.List;
import java.util.TreeMap;

public class Options_fragment extends Fragment_menu {

    private static Options_fragment this_fragment = null;
    private List<Option_fragment> listOfOptions;

    private TextView titleUser;
    private TextView editUser;

    public static Options_fragment getIntance(){
        if (this_fragment == null){
            this_fragment = new Options_fragment();
        }
        return this_fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_options_fragment, parent, false);
    }

    @Override
    public void onResume(){
        editUser.setTextColor(getResources().getColor(R.color.grey));
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        listOfOptions = OptionsManager.getInstance().getListOfOptions();

        ListView listViewStores = (ListView) view.findViewById(R.id.listOptionsView);
        titleUser = (TextView) view.findViewById(R.id.title);
        titleUser.setText(UserManager.getInstance().getName());

        editUser = (TextView) view.findViewById(R.id.editUser);

        editUser.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InterfaceUtils.showMensage("Logo irá abrir o editar perfil", Options_fragment.getIntance().getContext());
                editUser.setTextColor(getResources().getColor(R.color.colorPrimary));
            }
        });


        ListOptionsAdapter adapter = new ListOptionsAdapter(listOfOptions, this.getActivity());

        listViewStores.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InterfaceUtils.showMensage("Logo irá abrir a "+listOfOptions.get(position).getFragmentName(), Options_fragment.getIntance().getContext());
                //openDetails(listOfOptions.get(position));
            }

        });

        listViewStores.setAdapter(adapter);
    }

}
