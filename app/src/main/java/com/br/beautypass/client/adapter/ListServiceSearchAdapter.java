package com.br.beautypass.client.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.br.beautypass.client.R;
import com.br.beautypass.client.fragments.search.Search_by_title;
import com.br.beautypass.model.Service;
import com.br.beautypass.model.Store;
import com.br.beautypass.util.MVPUtils;

import java.util.List;

public class ListServiceSearchAdapter  extends BaseAdapter {

    private List<Service> listOfServices;
    private final Search_by_title act;

    public ListServiceSearchAdapter(List<Service> services, Search_by_title act) {
        this.listOfServices = services;
        this.act = act;
    }

    @Override
    public int getCount() {
        return listOfServices.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfServices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.activity_item_list_service_search, parent, false);
        Service service = listOfServices.get(position);

        //pegando as referencias das Views
        TextView nome = (TextView) view.findViewById(R.id.title);
        nome.setText(service.getTitle());

        return view;
    }
}
