package com.br.beautypass.client.fragments.details.services;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.br.beautypass.client.Home;
import com.br.beautypass.client.R;
import com.br.beautypass.client.service.Details_service;
import com.br.beautypass.client.service.Services_filtred_by_segment;
import com.br.beautypass.client.adapter.ListSegmentAdapter;
import com.br.beautypass.client.adapter.ListServicesAdapter;
import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.client.fragments.details.Details_Element;
import com.br.beautypass.model.Category;
import com.br.beautypass.model.Service;
import com.br.beautypass.model.Store;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class List_Services_fragment extends Details_Element {

    private Store store;
    private List<Service> listHighLight;
    private List<Service> listedServices;
    private List<Category> listOfSegments;

    public List_Services_fragment(Store store, Fragment_menu lastWindow){
        this.store = store;
        super.lastWindow = lastWindow;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_list_services, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        TextView titleListHighLight = (TextView) view.findViewById(R.id.title_highlight);
        ListView listViewHighLight = (ListView) view.findViewById(R.id.listServicesHighLight);

        TextView titleListSegments = (TextView) view.findViewById(R.id.title_segments);
        ListView listViewSegments = (ListView) view.findViewById(R.id.listSegments);

        //Fitra as listas
        loadLists();
        boolean empty = listOfSegments.isEmpty() || listHighLight.isEmpty();

        if (empty || !listHighLight.isEmpty()) {

            if (listHighLight.isEmpty()){
                listedServices = this.store.getListOfServices();
                titleListHighLight.setText(R.string.all_services);
                titleListSegments.setVisibility(View.GONE);
                listViewSegments.setVisibility(View.GONE);
            }else{
                listedServices = listHighLight;
            }

            //Altera a altura da lista
            ViewGroup.LayoutParams params = listViewHighLight.getLayoutParams();
            params.height = 236 * listedServices.size();
            listViewHighLight.setLayoutParams(params);
            listViewHighLight.setScrollContainer(false);

            ListServicesAdapter adapter = new ListServicesAdapter(listedServices, this.getActivity());

            listViewHighLight.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent sendIntent = new Intent(Home.getHome(), Details_service.class);
                    Bundle bundleInf= new Bundle();
                    bundleInf.putSerializable("store", store);
                    bundleInf.putSerializable("service", listedServices.get(position));
                    sendIntent.putExtras(bundleInf);
                    startActivity(sendIntent);
                }

            });

            listViewHighLight.setAdapter(adapter);
        }

        if (!listOfSegments.isEmpty()){

            //Altera a altura da lista
            ViewGroup.LayoutParams params = listViewSegments.getLayoutParams();
            params.height = 160 * listOfSegments.size();
            listViewSegments.setLayoutParams(params);
            listViewSegments.setScrollContainer(false);

            ListSegmentAdapter adapter = new ListSegmentAdapter(listOfSegments, store, this.getActivity());

            listViewSegments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent sendIntent = new Intent(Home.getHome(), Services_filtred_by_segment.class);
                    Bundle bundleInf= new Bundle();
                    bundleInf.putSerializable("store", store);
                    bundleInf.putSerializable("segment", listOfSegments.get(position));
                    sendIntent.putExtras(bundleInf);
                    startActivity(sendIntent);

                }

            });

            listViewSegments.setAdapter(adapter);

        }else{
            titleListSegments.setVisibility(View.GONE);
            listViewSegments.setVisibility(View.GONE);
        }
    }

    public Fragment_menu getThis(){
        return this;
    }

    private void loadLists(){
        listHighLight = new ArrayList<Service>();
        HashMap<Integer, Category> hashSegments = new HashMap<Integer, Category>();
        for (Service service : store.getListOfServices()){
            if (service.getSegment() != null)
                hashSegments.put(service.getSegment().getId(), service.getSegment());
            if (service.isHightlight())
                listHighLight.add(service);
        }

        listOfSegments = new ArrayList<Category>();
        if (!hashSegments.isEmpty())
            listOfSegments.addAll(hashSegments.values());
    }

    @Override
    public void onResume(){
        //ATUALIZAR A CADA RESUME
        super.onResume();
    }
}
