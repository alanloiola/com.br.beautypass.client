package com.br.beautypass.client.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.beautypass.client.R;
import com.br.beautypass.client.fragments.options.Option_fragment;

import java.util.List;

public class ListOptionsAdapter  extends BaseAdapter {

    private List<Option_fragment> listOfFragments;
    private final Activity act;

    public ListOptionsAdapter(List<Option_fragment> listOfFragments, Activity act) {
        this.listOfFragments = listOfFragments;
        this.act = act;
    }

    @Override
    public int getCount() {
        return listOfFragments.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfFragments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.activity_item_list_options, parent, false);
        Option_fragment fragment = listOfFragments.get(position);

        //pegando as referencias das Views
        TextView nome = (TextView) view.findViewById(R.id.title);
        ImageView imagem = (ImageView) view.findViewById(R.id.image);

        //populando as Views
        nome.setText(fragment.getFragmentName());
        imagem.setImageResource(fragment.getIcon());

        return view;
    }

}