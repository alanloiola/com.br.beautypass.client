package com.br.beautypass.client.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;

import com.br.beautypass.client.R;
import com.br.beautypass.model.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ListServicesAdapter extends BaseAdapter {

    private List<Service> listOfServices;
    private final Activity act;

    public ListServicesAdapter(List<Service> listOfServices, Activity act) {
        this.act = act;
        this.listOfServices = listOfServices;
    }

    @Override
    public int getCount() {
        return listOfServices.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfServices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.activity_item_list_services, parent, false);

        Service fragment = listOfServices.get(position);

        //pegando as referencias das Views
        TextView nome = (TextView) view.findViewById(R.id.serviceName);
        ImageView imagem = (ImageView) view.findViewById(R.id.imageService);

        @DrawableRes int image;
        int value = (new Random()).nextInt(3);
        switch (value){
            case 0:
                image = R.drawable.cabelo1;
                break;
            default:
                image = R.drawable.cabelo2;
                break;
        }
        //populando as Views
        nome.setText(fragment.getTitle());
        imagem.setImageResource(image);

        return view;
    }

}