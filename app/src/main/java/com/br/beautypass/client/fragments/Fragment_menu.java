package com.br.beautypass.client.fragments;

import androidx.fragment.app.Fragment;

import com.br.beautypass.client.Home;
import com.br.beautypass.client.fragments.details.calendar.Detail_calendar;
import com.br.beautypass.client.fragments.details.store.Detail_store_fragment;
import com.br.beautypass.client.fragments.details.Details_Element;
import com.br.beautypass.client.fragments.infoFragment.Error_fragment;
import com.br.beautypass.client.fragments.options.Option_fragment;
import com.br.beautypass.client.fragments.search.Filtred_Type_Serice;
import com.br.beautypass.model.Category;
import com.br.beautypass.model.ScheduledService;
import com.br.beautypass.model.Store;
import com.br.beautypass.util.Logger;

import java.io.Serializable;

public class Fragment_menu extends Fragment implements Serializable{

    protected int fragmentId;
    protected String fragmentName;

    protected Fragment_menu info_fragment;
    protected Details_Element detail_page = null;
    protected Fragment_menu lastWindow;

    protected boolean errorOrLoading = false,
                    load = false;

    public boolean isReady(){
        return true;
    }

    protected void processRequirements(){
    }

    public Fragment_menu getLoading_fragment(){
        return this.info_fragment;
    }

    /**
     * Obtem o fragmento com validação se está em loading ou erro
     * @return
     */
    public Fragment_menu getFragment() {

        //Valida se está pronto para ser exibido
        if (!isReady() && !load) {
            processRequirements();
        }

        //Valida se retorna a mensagem de erro/loading ou o fragmento atual
        if (errorOrLoading && info_fragment != null) {
            return info_fragment;
        } else if (detail_page != null
                && Home.getHome().getSelectedId() != this.fragmentId){
            return detail_page;
        }else{
            finalizeDetails();
            return this;
        }
    }

    /**
     * Metodo de retorno quando se carregado o fragmento com sucesso
     */
    public void callBackReady(){
        if (Home.getHome() != null){
            errorOrLoading = false;
            load = true;
            if (isReady())
                Home.getHome().updateFragmentIfSelected(this);
        }
    }

    /**
     * Metodo de retorno quando se carregado o fragmento com erro
     * @param messageError - mensagem de erro à ser exibido
     */
    public void callBackError(String messageError){
        info_fragment = new Error_fragment(messageError);
        info_fragment.setFragmentId(this.getFragmentId());
        errorOrLoading = true;
        load = false;
        Home.getHome().updateFragmentIfSelected(info_fragment);
    }

    public void backFromDetails(){
        if (this.detail_page != null) {
            finalizeDetails();
            Home.getHome().updateFragmentIfSelected(getFragment());
        }
    }

    private void finalizeDetails(){
        if (this.detail_page != null) {
            try {
                this.detail_page.finalize();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                Logger.erroMessage("Erro ao voltar dos detalhes");
            }
            this.detail_page = null;
        }
    }

    public void setFragmentId(int fragmentId){
        this.fragmentId = fragmentId;
    }

    public int getFragmentId(){
        return this.fragmentId;
    }

    public void openDetails(Store store){
        detail_page = new Detail_store_fragment(store, this);
        detail_page.setFragmentId(this.getFragmentId());
        Home.getHome().updateFragmentIfSelected(detail_page);
    }

    public void openDetails(Category category){
        detail_page = new Filtred_Type_Serice(category, this);
        detail_page.setFragmentId(this.getFragmentId());
        Home.getHome().updateFragmentIfSelected(detail_page);
    }

    public void openDetails(ScheduledService sheduledService){
        detail_page = new Detail_calendar(sheduledService, this);
        detail_page.setFragmentId(this.getFragmentId());
        Home.getHome().updateFragmentIfSelected(detail_page);
    }

    public void onRequestPermissionsResult() {
    }

    public String getFragmentName(){
        return fragmentName;
    }

    public void setFragmentName(String fragmentName){
        this.fragmentName = fragmentName;
    }


}
