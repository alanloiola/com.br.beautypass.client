package com.br.beautypass.client.fragments.menu;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.br.beautypass.client.Home;
import com.br.beautypass.client.R;
import com.br.beautypass.client.adapter.ListServiceSearchAdapter;
import com.br.beautypass.client.adapter.ListServicesAdapter;
import com.br.beautypass.client.adapter.ListStoreAdapter;
import com.br.beautypass.client.adapter.ListTypeStoreAdapterView;
import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.client.fragments.details.calendar.Detail_calendar;
import com.br.beautypass.client.fragments.infoFragment.Loading_fragment;
import com.br.beautypass.client.fragments.search.List_All_Stores;
import com.br.beautypass.client.fragments.search.Search_by_title;
import com.br.beautypass.client.service.Details_service;
import com.br.beautypass.manager.StoreManager;
import com.br.beautypass.manager.UserExperience;
import com.br.beautypass.model.HomeConfiguration;
import com.br.beautypass.model.ScheduledService;
import com.br.beautypass.model.Service;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class Home_fragment extends Fragment_menu {

    private static Home_fragment this_fragment = null;
    private HomeConfiguration homeConfiguration;
    private RelativeLayout servicesRecomended;
    private EditText search;

    private ArrayList<Service> listServicesRecommeneded;
    private ListView listServices;

    public static Home_fragment getIntance(){
        if (this_fragment == null){
            this_fragment = new Home_fragment();
        }
        return this_fragment;
    }

    @Override
    public boolean isReady(){
        boolean ready = false;
        if (StoreManager.getInstance().isIsReady()){
            ready =  true;
        }else if (super.info_fragment == null){
            super.info_fragment = new Loading_fragment();
        }
        return ready;
    }

    @Override
    protected void processRequirements(){
        super.errorOrLoading = true;
        StoreManager.getInstance().initProcess(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_home_fragment, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        //Fipper da home
        ViewFlipper viewFlipper = (ViewFlipper) view.findViewById(R.id.viewFlipper1);
        //viewFlipper.setOnClickListener(this);
        viewFlipper.setFlipInterval(10000);
        viewFlipper.startFlipping();

        homeConfiguration = StoreManager.getInstance().getHomeConfiguration();
        ListView listViewStores = (ListView) view.findViewById(R.id.listStoresView);

        ListStoreAdapter adapter = new ListStoreAdapter(homeConfiguration.getListOfStoresHome(), this.getActivity());
        listViewStores.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openDetails(homeConfiguration.getListOfStoresHome().get(position));
            }
        });

        //Altera a altura da lista
        ViewGroup.LayoutParams params = listViewStores.getLayoutParams();
        params.height = 236 * homeConfiguration.getListOfStoresHome().size();
        listViewStores.setLayoutParams(params);
        listViewStores.setScrollContainer(false);
        listViewStores.setAdapter(adapter);

        //Serviços recomendados
        servicesRecomended = (RelativeLayout) view.findViewById(R.id.finalize_services);
        listServices = (ListView) view.findViewById(R.id.listFinalizeServices);
        renderRecomendedServices();

        RecyclerView listCategorys = (RecyclerView) view.findViewById(R.id.listSegments);
        ListTypeStoreAdapterView adapter2 = new ListTypeStoreAdapterView(this.getContext(), homeConfiguration.getListOfCategorysHome(), this);
        listCategorys.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.HORIZONTAL, false));
        listCategorys.setAdapter(adapter2);

        TextView seeAllStores = (TextView) view.findViewById(R.id.buttonSeeMoreClosest);
        seeAllStores.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openDetailsAllStores();
            }
        });

        search = (EditText) view.findViewById(R.id.search);
        search.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    goToSearch();
                    return true;
                }
                return false;
            }
        });

        search.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (search.getRight() - search.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        goToSearch();
                        return true;
                    }
                }
                return false;
            }
        });

    }

    private void renderRecomendedServices(){
        listServicesRecommeneded = UserExperience.getListOfServicesUser();
        if (!listServicesRecommeneded.isEmpty()){
            servicesRecomended.setVisibility(View.VISIBLE);

            //Altera a altura da lista
            ViewGroup.LayoutParams paramsServices = listServices.getLayoutParams();
            paramsServices.height = 236 * listServicesRecommeneded.size();
            listServices.setLayoutParams(paramsServices);
            listServices.setScrollContainer(false);

            ListServicesAdapter listAdapter = new ListServicesAdapter(listServicesRecommeneded, this.getActivity());
            listServices.setAdapter(listAdapter);

            listServices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent sendIntent = new Intent(Home.getHome(), Details_service.class);
                    Bundle bundleInf= new Bundle();
                    bundleInf.putSerializable("store", listServicesRecommeneded.get(position).getStore());
                    bundleInf.putSerializable("service", listServicesRecommeneded.get(position));
                    sendIntent.putExtras(bundleInf);
                    startActivity(sendIntent);
                }

            });

        }else{
            servicesRecomended.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume(){
        search.setText("");
        renderRecomendedServices();
        super.onResume();
    }

    public void goToSearch(){
        String searchTxt = search.getText().toString();
        if (!searchTxt.isEmpty()) {
            detail_page = new Search_by_title(this, search.getText().toString());
            detail_page.setFragmentId(this.getFragmentId());
            Home.getHome().updateFragmentIfSelected(detail_page);
        }
    }

    public void openDetailsAllStores(){
        detail_page = new List_All_Stores(this);
        detail_page.setFragmentId(this.getFragmentId());
        Home.getHome().updateFragmentIfSelected(detail_page);
    }

    @Override
    public void backFromDetails(){
        search.setText("");
        super.backFromDetails();
    }

}
