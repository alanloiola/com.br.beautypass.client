package com.br.beautypass.client.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.br.beautypass.client.R;
import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.model.Category;
import com.br.beautypass.util.MVPUtils;

import java.util.List;

public class ListTypeStoreAdapterView extends RecyclerView.Adapter<ListTypeStoreAdapterView.TypeServieViewHolder>{

    private Context context;
    private Fragment_menu window;
    private List<Category> items;

    public ListTypeStoreAdapterView(Context context, List<Category> items, Fragment_menu window) {
        this.context = context;
        this.items = items;
        this.window = window;
    }

    @NonNull
    @Override
    public ListTypeStoreAdapterView.TypeServieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TypeServieViewHolder(LayoutInflater.from(context).inflate(R.layout.activity_item_list_category_store,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TypeServieViewHolder holder, int position) {
        holder.getItemTitle().setText(items.get(position).getName());
        holder.getItemImage().setImageResource(MVPUtils.getImageTypeStoreById(items.get(position).getImageId()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void openDetails(int position){
        window.openDetails(items.get(position));
    }

    public class TypeServieViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{

        private ImageView itemImage;
        private TextView itemTitle;

        public TypeServieViewHolder(View view) {
            super(view);
            itemImage = view.findViewById(R.id.image);
            itemTitle = view.findViewById(R.id.title);
            view.setOnClickListener(this);
        }

        public ImageView getItemImage() {
            return itemImage;
        }

        public void setItemImage(ImageView itemImage) {
            this.itemImage = itemImage;
        }

        public TextView getItemTitle() {
            return itemTitle;
        }

        public void setItemTitle(TextView itemTitle) {
            this.itemTitle = itemTitle;
        }

        @Override
        public void onClick(View v) {
            openDetails(getAdapterPosition());
        }
    }

}