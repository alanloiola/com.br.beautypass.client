package com.br.beautypass.client.fragments.details.services;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.Nullable;

import com.br.beautypass.client.R;
import com.br.beautypass.client.fragments.details.Details_Element;
import com.br.beautypass.client.service.Details_service;
import com.br.beautypass.model.DateService;

import java.util.ArrayList;

public class Date_select_service extends Details_Element{

    private Details_service details_page;
    private Spinner spinnerTime;
    private ArrayList<DateService> listOfDate;

    public Date_select_service(Details_service details_page, ArrayList<DateService> listOfDate) {
        this.details_page = details_page;
        this.listOfDate = listOfDate;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_select_date_service, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        //listOfDate = DateManager.getInstance().getServicesDate(details_page.getService().getId());

        ArrayList<String> listOfDatesStr = new ArrayList<>();
        for (DateService dateService : listOfDate){
            if (!dateService.getListOfTime().isEmpty())
                listOfDatesStr.add(dateService.getDate());
        }

        Spinner spinnerDate = (Spinner) view.findViewById(R.id.spinnerDate);
        ArrayAdapter<String> spinnerAdapterDate =
                new ArrayAdapter<String>(details_page, android.R.layout.simple_spinner_item, listOfDatesStr);

        spinnerAdapterDate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDate.setAdapter(spinnerAdapterDate);

        //Evento de alteração do spinner
        spinnerDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                changeDateSelected(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spinnerTime = (Spinner) view.findViewById(R.id.spinnerHour);
        for (int index = 0; index < listOfDate.size(); index++) {
            if (!listOfDate.get(index).getListOfTime().isEmpty()){
                changeDateSelected(index);
                break;
            }
        }

        spinnerTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                changeTimeSelected(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

    }

    public void changeDateSelected(int position){
        DateService dateSelecected = listOfDate.get(position);
        details_page.setDateSelected(dateSelecected);
        ArrayList<String> listHour = dateSelecected.getTimeString();

        ArrayAdapter<String> spinnerAdapterTime =
                new ArrayAdapter<String>(details_page, android.R.layout.simple_spinner_item, listHour);

        spinnerAdapterTime.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTime.setAdapter(spinnerAdapterTime);
    }

    public void changeTimeSelected(int position){
        details_page.setTimeSelected(position);
    }


}
