package com.br.beautypass.client.fragments.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.br.beautypass.client.Home;
import com.br.beautypass.client.R;
import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.client.fragments.infoFragment.Loading_fragment;
import com.br.beautypass.manager.MapManager;
import com.br.beautypass.manager.StoreManager;
import com.br.beautypass.manager.UserManager;
import com.br.beautypass.model.Store;
import com.br.beautypass.util.InterfaceUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class  Map_fragment extends Fragment_menu implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, OnMarkerClickListener, OnInfoWindowClickListener, GoogleMap.OnCameraChangeListener {

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private CameraPosition lastCameraPosition = null;

    private MapView mMapView;
    private GoogleMap googleMap;
    private Bundle mBundle;

    private EditText searchText;

    //Armazena o a loja e seu respsctivo id
    HashMap<String, Store> listOfStores = new HashMap<String, Store>();
    private HashMap<String, Marker> listOfMarkers = new HashMap<String, Marker>();

    private static Map_fragment this_fragment = null;

    public static Map_fragment getIntance() {
        if (this_fragment == null) {
            this_fragment = new Map_fragment();
        }
        return this_fragment;
    }

    @Override
    public boolean isReady() {
        boolean ready = false;
        boolean hasPermitions = InterfaceUtils.hasMapPermitions(Home.getHome().getApplicationContext());
        if (!hasPermitions) {
            super.callBackError("Habilite as permissões de localização para continuar");
        } else if (StoreManager.getInstance().isIsReady()
                && StoreManager.getInstance().getErrorMessage() == null) {
            ready = true;
        } else if (StoreManager.getInstance().isIsReady() && StoreManager.getInstance().getErrorMessage() != null) {
            super.callBackError(StoreManager.getInstance().getErrorMessage());
        } else if (super.info_fragment == null) {
            super.info_fragment = new Loading_fragment();
        }
        return ready;
    }

    @Override
    protected void processRequirements() {
        super.errorOrLoading = true;
        if (!StoreManager.getInstance().isIsReady())
            StoreManager.getInstance().initProcess(this);
        if (!InterfaceUtils.hasMapPermitions(Home.getHome().getApplicationContext()))
            InterfaceUtils.getPermissions(Home.getHome());
    }


    @Override
    public void onRequestPermissionsResult() {
        if (InterfaceUtils.hasMapPermitions(Home.getHome().getApplicationContext())) {
            StoreManager.getInstance().initProcess(this);
            super.callBackReady();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)
                .setFastestInterval(1 * 1000);
    }

    @Override
    //Called when the UI is ready to draw the Fragment
    //this method must return a View that is the root of your fragment's layout.
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View inflatedView = inflater.inflate(R.layout.activity_map_fragment, container, false);

        mMapView = (MapView) inflatedView.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        mMapView.onCreate(mBundle);

        searchText = (EditText) inflatedView.findViewById(R.id.search);
        searchText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    search();
                    return true;
                }
                return false;
            }
        });

        searchText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (searchText.getRight() - searchText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        search();
                        return true;
                    }
                }
                return false;
            }
        });

        return inflatedView;
    }


    @Override
    public void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
        mMapView.onResume();
        setUpMap();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        googleMap = null;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    private void setUpMap() {

        if (googleMap == null)
            mMapView.getMapAsync(new OnMapReadyCallback() {

                @Override
                public void onMapReady(GoogleMap googleMap) {
                    if (googleMap != null) {
                        configMap(googleMap);
                    }
                }
            });

    }

    public void configMap(GoogleMap googleMap) {

        this.googleMap = googleMap;

        //Show current location
        this.googleMap.setMyLocationEnabled(true);

        View location_button = mMapView.findViewWithTag("GoogleMapMyLocationButton");
        View zoom_in_button = mMapView.findViewWithTag("GoogleMapZoomInButton");
        View zoom_layout = (View) zoom_in_button.getParent();

        // adjust location button layout params above the zoom layout
        RelativeLayout.LayoutParams location_layout = (RelativeLayout.LayoutParams) location_button.getLayoutParams();
        location_layout.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        location_layout.addRule(RelativeLayout.ABOVE, zoom_layout.getId());

        //Google maps zoom buttons
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);

        //Zoom gestures
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);

        //Don't set a My Location button on Google Maps
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        //Compass
        this.googleMap.getUiSettings().setCompassEnabled(true);

        //Set marker click listener as this one
        this.googleMap.setOnMarkerClickListener(this);

        //Set annotation view click listener as this one
        this.googleMap.setOnInfoWindowClickListener(this);

        this.googleMap.setOnCameraChangeListener(this);

        this.googleMap.setBuildingsEnabled(false);

        //Populate map with markers
        addMarkersToMap();

        if (lastCameraPosition == null)
            moveCameraToLocation(UserManager.getInstance().getLatLngUser());
        else
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lastCameraPosition.target, lastCameraPosition.zoom));
    }

    public void moveCameraToLocation(LatLng latLng) {
        if (googleMap != null) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .zoom(17)
                    .target(latLng)// Sets the orientation of the camera to east
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    private void addMarkersToMap() {

        //Marker to be added to the map
        Marker marker;
        ArrayList<Store> stores = StoreManager.getInstance().getNearListStores();

        if (stores != null) {
            for (Store store : stores) {
                if (!listOfStores.containsKey(store.getId())) {
                    marker = googleMap.addMarker(new MarkerOptions().position(store.getLatLng()).title(store.getTitle()).snippet(store.getIntroduction())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.scissors_icon_black)));
                
                    listOfMarkers.put(marker.getId(), marker);
                    listOfStores.put(marker.getId(), store);
                }
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION))
            return;

        if (lastCameraPosition == null) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location == null)
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) this);
            else
                moveCameraToLocation(new LatLng(location.getLatitude(), location.getLongitude()));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private boolean hasPermission(String permission) {
        return ContextCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

        Store storeSelected = listOfStores.get(marker.getId());

        if (storeSelected == null) {
            InterfaceUtils.showMensage("Erro ao obter o detalhe da loja", getContext());
        } else {
            super.openDetails(storeSelected);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        this.lastCameraPosition = cameraPosition;
    }

    private void search(){
        LatLng resultSearch = MapManager.getInstance().process(searchText.getText().toString(), this);
        if (resultSearch != null){
            moveCameraToLocation(resultSearch);
        }else{
            InterfaceUtils.showMensage("Erro ao obter a localização", this.getContext());
        }
    }

}