package com.br.beautypass.client.service;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.beautypass.client.R;
import com.br.beautypass.manager.GoogleCalendarManager;
import com.br.beautypass.model.GoogleCalendarEvent;
import com.br.beautypass.util.InterfaceUtils;

public class Default_Message extends AppCompatActivity {

    private GoogleCalendarEvent googleCalendarEvent;
    private boolean loading = false, sucess = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default_message);

        Button calendar = (Button) findViewById(R.id.addToCalendar);
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToHome();
            }
        });

        TextView message= (TextView) findViewById(R.id.message);
        ImageView imageView = (ImageView) findViewById(R.id.image);

        //Recebe as configurações
        Intent intentRecebida = getIntent();
        if (intentRecebida!=null) {
            Bundle bundle = intentRecebida.getExtras();
            if (bundle != null) {

                Object errorObj = bundle.get("error");
                Object messageObj = bundle.get("msg");
                Object calendarObj = bundle.get("calendarObj");

                if (messageObj != null && errorObj != null){

                    if (errorObj.toString() != "true"){
                        imageView.setImageResource(R.drawable.sucess);
                    }

                    message.setText(messageObj.toString());

                    if (calendarObj == null){
                        calendar.setVisibility(View.GONE);
                    }else{
                        googleCalendarEvent = (GoogleCalendarEvent) calendarObj;
                        calendar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                goToCalendar();
                            }
                        });
                    }

                }

            }
        }

    }

    public void goToCalendar(){
        if (InterfaceUtils.hasCalendarPermitions(this)) {
            loading = true;
            GoogleCalendarManager.sendToCalendar(this, googleCalendarEvent);
        } else {
            InterfaceUtils.getPermissions(this);
        }
    }

    public void goToHome(){
        this.finish();
    }

    @Override
    public void onBackPressed() {
        goToHome();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        loading = true;
        GoogleCalendarManager.sendToCalendar(this, googleCalendarEvent);
    }

    public void callBackSucess(){
        loading = false;
        sucess = true;
        InterfaceUtils.showMensage("Agendamento realizado com sucesso!", this);
    }

    public void callBackError(){
        loading = false;
        InterfaceUtils.showMensage("Erro ao tentar realizar o agendamento", this);
    }
}
