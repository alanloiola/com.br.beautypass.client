package com.br.beautypass.client.fragments.search;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.br.beautypass.client.Home;
import com.br.beautypass.client.R;
import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.client.fragments.details.Details_Element;
import com.br.beautypass.client.fragments.details.store.Detail_store_fragment;
import com.br.beautypass.client.fragments.infoFragment.Error_fragment;
import com.br.beautypass.client.fragments.infoFragment.Loading_fragment;
import com.br.beautypass.manager.SearchManager;
import com.br.beautypass.model.SearchConfiguration;
import com.br.beautypass.model.Store;

public class Search_by_title extends Details_Element {

    private String searchText;
    private EditText searchField;
    private SearchConfiguration searchConfiguration;

    public Search_by_title(Fragment_menu lastWindow, String searchText){
        super.lastWindow = lastWindow;
        this.searchText = searchText;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_list_stores_filtred_search, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        ImageView backImage = (ImageView) view.findViewById(R.id.back);
        backImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                backToFatherWindow();
            }
        });

        searchField = (EditText) view.findViewById(R.id.search);

        searchField.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    doSearch();
                    return true;
                }
                return false;
            }
        });

        searchField.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (searchField.getRight() - searchField.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        doSearch();
                        return true;
                    }
                }
                return false;
            }
        });

        if (searchText != null){
            searchField.setText(searchText);
            doSearch();
        }

    }

    private void doSearch(){
        if (searchField.getText().toString().trim().isEmpty()){
            callBackError("Digite algo para buscar");
        }else{
            this.info_fragment = new Loading_fragment();
            changeFragment(info_fragment);
            SearchManager.getInstance().process(searchField.getText().toString(), this);
        }
    }

    public void callBackReady(SearchConfiguration searchConfiguration){
        errorOrLoading = false;
        load = true;
        this.searchConfiguration = searchConfiguration;
        List_result_search_fragment list_result_search_fragment = new List_result_search_fragment(this);
        changeFragment(list_result_search_fragment);
    }

    @Override
    public void callBackError(String messageError){
        info_fragment = new Error_fragment(messageError);
        info_fragment.setFragmentId(this.getFragmentId());
        errorOrLoading = true;
        load = false;
        changeFragment(info_fragment);
    }

    private void changeFragment(Fragment_menu toChange){
        this.getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameList, toChange) // replace flContainer
                .addToBackStack(null)
                .commitAllowingStateLoss();;
    }

    public void openStore(Store store){
        detail_page = new Detail_store_fragment(store, this);
        detail_page.setFragmentId(this.getFragmentId());
        Home.getHome().updateFragmentIfSelected(detail_page);
    }

    public SearchConfiguration getSearchConfiguration() {
        return searchConfiguration;
    }
}
