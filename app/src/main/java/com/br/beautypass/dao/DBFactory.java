package com.br.beautypass.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBFactory extends SQLiteOpenHelper {

    private static final String BD_NAME = "beautydbclient.db";
    private static final int VERSAO = 2;

    public DBFactory(@Nullable Context context) {
        super(context, BD_NAME, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableController = "CREATE TABLE IF NOT EXISTS USER (" +
                "id INTEGER," +
                "name VARCHAR(255) NOT NULL," +
                "PRIMARY KEY(id) );";
        db.execSQL(createTableController);

        String createTableJSON = "CREATE TABLE IF NOT EXISTS CACHE_JSON (" +
                "id VARCHAR(255) NOT NULL," +
                "content LONGTEXT NOT NULL," +
                "PRIMARY KEY(id) );";
        db.execSQL(createTableJSON);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (oldVersion == 1){
            String createTableJSON = "CREATE TABLE IF NOT EXISTS CACHE_JSON (" +
                    "id VARCHAR(255) NOT NULL," +
                    "content LONGTEXT NOT NULL," +
                    "PRIMARY KEY(id) );";
            db.execSQL(createTableJSON);
        }

    }

}
