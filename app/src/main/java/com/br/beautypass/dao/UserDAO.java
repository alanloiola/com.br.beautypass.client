package com.br.beautypass.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.br.beautypass.model.User;

public class UserDAO {

    private DBFactory database;

    public UserDAO(Context context){
        database = new DBFactory(context);
    }

    public User getUser(){
        String selectQuery = "SELECT * FROM USER";

        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        User userToReturn = null;
        if(cursor.getCount() > 0) {
            StringBuilder sb = new StringBuilder();
            cursor.moveToFirst();

            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            userToReturn = new User(id, name);

        }

        cursor.close();
        db.close();

        return userToReturn;
    }

    public boolean insertUser(User userToInsert){
        ContentValues values;
        boolean sucess = true;

        try {
            SQLiteDatabase db = database.getWritableDatabase();
            values = new ContentValues();

            values.put("name", userToInsert.getName());
            values.put("id", userToInsert.getId());

            long resultado = db.insert("USER", null, values);

            db.close();
            if (resultado == -1)
                sucess = false;

        } catch (Exception e){
            System.out.println(e);
            sucess = false;
        }

        return sucess;
    }


}
