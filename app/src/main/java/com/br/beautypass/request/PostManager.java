package com.br.beautypass.request;


import org.json.simple.JSONObject;

public class PostManager extends  PostServer {

    public JSONObject postServiceTime(JSONObject jsonPost){
        return super.sendPost(jsonPost.toString(), "/serviceCalendar");
    }

    public JSONObject postContact(JSONObject jsonPost){
        return super.sendPost(jsonPost.toString(), "/contact");
    }

    public JSONObject postRating(JSONObject jsonPost){
        return super.sendPost(jsonPost.toString(), "/ratingService");
    }

    public JSONObject postUserExperience(JSONObject jsonPost){
        return super.sendPost(jsonPost.toString(), "/userExperience");
    }

}
