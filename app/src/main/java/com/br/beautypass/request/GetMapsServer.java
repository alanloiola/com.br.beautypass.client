package com.br.beautypass.request;

import android.os.StrictMode;

import com.br.beautypass.client.Home;
import com.br.beautypass.client.R;
import com.br.beautypass.util.RequestUtils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Set;

public class GetMapsServer {

    private String URL = "https://maps.googleapis.com/maps/api/geocode/json";

    public JSONObject getJSONByURL(String adress) {

        JSONObject jsonToReturn = null;
        InputStream inputStream = null;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String id = Home.getHome().getString(R.string.google_maps_key);
        String urlToRequest = URL+ "?key="+id+"&address="+encodeValue(adress);

        String contentType = null;
        HttpURLConnection apiInfoConnection;

        try {

            apiInfoConnection = (HttpURLConnection) new URL(urlToRequest).openConnection();
            apiInfoConnection.setRequestMethod("GET");

            if (apiInfoConnection != null && apiInfoConnection.getResponseCode() != 200) {
                return null;
            }
            inputStream = apiInfoConnection.getInputStream();
            contentType = apiInfoConnection.getHeaderField("Content-Type");
        } catch (Exception e1) {
            e1.printStackTrace();
            return null;
        }

        if (inputStream != null) {

            String charSet = RequestUtils.getCharSet(contentType);
            //Obtem o JSON
            try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(inputStream, charSet))) {
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(fileReader);
                jsonToReturn = (JSONObject) obj;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jsonToReturn;
    }

    private static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }

}
