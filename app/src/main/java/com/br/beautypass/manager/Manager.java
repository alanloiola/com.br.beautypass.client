package com.br.beautypass.manager;

import com.br.beautypass.client.fragments.Fragment_menu;

import java.util.ArrayList;

public class Manager {

    protected boolean isReady = false, loading = false;
    protected String errorMessage = null;

    protected ArrayList<Fragment_menu> fragments_to_call_back = new ArrayList<>();

    public boolean isIsReady(){
        return isReady;
    }

    public String getErrorMessage(){return errorMessage;}

    public void callFragmentSucess(){
        isReady = true;
        loading = false;
        if (fragments_to_call_back != null && !fragments_to_call_back.isEmpty()) {
            for (Fragment_menu fragment_menu : fragments_to_call_back)
                fragment_menu.callBackReady();
        }
    }

    public void callFragmentError(String message){
        isReady = true;
        loading = false;
        errorMessage = message;
        if (fragments_to_call_back != null && !fragments_to_call_back.isEmpty()){
            for (Fragment_menu fragment_menu : fragments_to_call_back)
                fragment_menu.callBackError(message);
        }
    }

    public void initProcess(Fragment_menu fragment){
        fragments_to_call_back.add(fragment);

        if (!loading) {
            loading = true;
            process();
        }
    }

    protected void process(){

    }

}
