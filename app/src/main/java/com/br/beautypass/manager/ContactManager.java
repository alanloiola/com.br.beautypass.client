package com.br.beautypass.manager;

import com.br.beautypass.client.Contact;
import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.request.PostManager;

import org.json.simple.JSONObject;


public class ContactManager extends Manager {

    private static ContactManager instance;
    private Contact contactCallBack;

    private String meessage;
    private int id;

    public static ContactManager getInstance(){
        if (instance == null){
            instance = new ContactManager();
        }
        return instance;
    }

    public void process(String message, int id, Contact contactCallBack){
        this.meessage = message;
        this.id = id;
        this.contactCallBack = contactCallBack;

        new Thread() {
            @Override
            public void run() {
                postContact();
            }
        }.start();
    }

    @Override
    public void callFragmentSucess(){
        contactCallBack.callBackSucess();
    }

    @Override
    public void callFragmentError(String message){
        contactCallBack.callBackError(message);
    }

    private void postContact(){

        PostManager postManager = new PostManager();

        JSONObject jsonToSend = new JSONObject();
        jsonToSend.put("id", id);
        jsonToSend.put("message", meessage);

        JSONObject jsonResponse = postManager.postContact(jsonToSend);
        if (jsonResponse != null){

            Object sucessObj = jsonResponse.get("sucess");
            Object text = jsonResponse.get("responseTxt");
            if (sucessObj != null){

                if (sucessObj.toString().equals("true")){
                    callFragmentSucess();
                }else{
                    if (text != null) {
                        callFragmentError(text.toString());
                    }else{
                        callFragmentError("Erro no servidor");
                    }
                }

            }else{
                callFragmentError("Erro ao formatar os dados no servidor");
            }

        }else{
            callFragmentError("Erro ao obter a resposta do servidor");
        }


    }


}