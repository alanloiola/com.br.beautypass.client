package com.br.beautypass.manager;

import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.client.fragments.search.Search_by_title;
import com.br.beautypass.model.Category;
import com.br.beautypass.model.HomeConfiguration;
import com.br.beautypass.model.SearchConfiguration;
import com.br.beautypass.model.Store;
import com.br.beautypass.request.RequestManager;
import com.br.beautypass.util.Logger;
import com.br.beautypass.util.StoreUtils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SearchManager extends Manager{

    private static SearchManager instance = null;

    public static SearchManager getInstance(){
        if (instance == null){
            instance = new SearchManager();
        }
        return instance;
    }

    public void callFragmentSucess(SearchConfiguration searchConfiguration){
        isReady = true;
        loading = false;
        if (fragments_to_call_back != null && !fragments_to_call_back.isEmpty()) {
            for (Fragment_menu fragment_menu : fragments_to_call_back)
                if (fragment_menu instanceof Search_by_title) {
                    ((Search_by_title)fragment_menu).callBackReady(searchConfiguration);
                }
            fragments_to_call_back.clear();
        }
    }

    public void process(final String searchText, Fragment_menu fragmentToCallBack){
        fragments_to_call_back.add(fragmentToCallBack);

        new Thread() {
            @Override
            public void run() {

                RequestManager requestServer = new RequestManager();
                JSONObject listOfStores = requestServer.getStores(searchText);
                if (listOfStores != null) {
                    setStores(listOfStores);
                } else {
                    SearchManager.getInstance().callFragmentError("Erro ao obter as lojas, contate o adminsitrador.");
                }

            }
        }.start();

    }

    private void setStores(JSONObject listStores) {

        try {
            boolean sucess = (Boolean)listStores.get("sucess");

            //Valida se o servidor retornou corretamente
            if (sucess){

                Long lengthStores = (Long)listStores.get("size");
                if (lengthStores > 0) {

                    SearchConfiguration searchConfiguration = buildStores(listStores);

                    //Valida se foi possível inserir as lojas
                    if (searchConfiguration.isEmpty()){
                        callFragmentError("Erro ao processar os dados das lojas");
                        Logger.erroMessage("Erro ao processar os dados das lojas");
                    }else {
                        callFragmentSucess(searchConfiguration);
                        isReady = true;
                    }
                }else{
                    callFragmentError("Não há resultados para a busca");
                    Logger.erroMessage("Servidor não retornou lojas");
                }

            }else{
                String erroMessage = (String)listStores.get("responseTxt");
                callFragmentError(erroMessage);
                Logger.erroMessage("Servidor: "+erroMessage);
            }

        }catch (Exception e){
            callFragmentError("Erro ao processar os dados das lojas");
            Logger.erroMessage(e, "Erro ao processar os dados das lojas");
            e.printStackTrace();
        }
    }

    private SearchConfiguration buildStores(JSONObject stores){

        SearchConfiguration searchConfiguration = new SearchConfiguration();

        ArrayList<Store> resultStores = new ArrayList<>();
        HashMap<Integer, Category> listOfCategorys = new HashMap<>();

        JSONArray listOfJsonStores = (JSONArray) stores.get("stores");

        for (int index = 0; index < listOfJsonStores.size(); index++){
            JSONObject jsonStore = (JSONObject) listOfJsonStores.get(index);
            Store store = StoreUtils.getStoreByJSON(jsonStore);
            if (store.isValid()) {
                resultStores.add(store);
                listOfCategorys.put(store.getCategory().getId(), store.getCategory());
            }
        }

        searchConfiguration.setResultStores(resultStores);

        //Define propriedades
        JSONObject proprieties = (JSONObject) stores.get("properties");
        if (proprieties.get("limitSearchServices") != null) {
            int limitServices = Integer.parseInt(proprieties.get("limitSearchServices").toString());
            searchConfiguration.setLimitServices(limitServices);
        }

        return searchConfiguration;
    }

}
