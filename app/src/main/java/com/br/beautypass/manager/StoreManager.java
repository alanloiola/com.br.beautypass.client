package com.br.beautypass.manager;

import com.br.beautypass.model.*;
import com.br.beautypass.request.RequestManager;
import com.br.beautypass.util.Logger;
import com.br.beautypass.util.StoreUtils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class StoreManager extends Manager{

    private HomeConfiguration homeConfiguration = null;
    private static StoreManager storeManager;

    public static StoreManager getInstance(){
        if (storeManager == null){
            storeManager = new StoreManager();
        }
        return storeManager;
    }

    @Override
    protected void process(){

        new Thread() {
            @Override
            public void run() {

                RequestManager requestServer = new RequestManager();
                JSONObject listOfStores = requestServer.getStores();
                if (listOfStores != null) {
                    setStores(listOfStores);
                } else {
                    StoreManager.getInstance().callFragmentError("Erro ao obter as lojas, contate o adminsitrador.");
                }

            }
        }.start();

    }

    /**
     * Recebe o JSON com as lojas e armazena em memoria
     * @param listStores
     */
    private void setStores(JSONObject listStores){

        try {
            boolean sucess = (Boolean)listStores.get("sucess");

            //Valida se o servidor retornou corretamente
            if (sucess){

                Long lengthStores = (Long)listStores.get("size");
                if (lengthStores > 0) {

                    buildStores(listStores);

                    //Valida se foi possível inserir as lojas
                    if (homeConfiguration.isEmpty()){
                        callFragmentError("Erro ao processar os dados das lojas");
                        Logger.erroMessage("Erro ao processar os dados das lojas");
                    }else {
                        StoreManager.getInstance().callFragmentSucess();
                        isReady = true;
                    }
                }else{
                    callFragmentError("Não há lojas cadastradas na sua região");
                    Logger.erroMessage("Servidor não retornou lojas");
                }

            }else{
                String erroMessage = (String)listStores.get("responseTxt");
                callFragmentError(erroMessage);
                Logger.erroMessage("Servidor: "+erroMessage);
            }

        }catch (Exception e){
            callFragmentError("Erro ao processar os dados das lojas");
            Logger.erroMessage(e, "Erro ao processar os dados das lojas");
            e.printStackTrace();
        }
    }

    /**
     * Controe o arrayList com o jsonObject de lojas
     * @param stores
     */
    private void buildStores(JSONObject stores){

        homeConfiguration = new HomeConfiguration();

        ArrayList<Store> nearListStores = new ArrayList<>();
        HashMap<Integer, Category> listOfCategorys = new HashMap<>();

        JSONArray listOfJsonStores = (JSONArray) stores.get("stores");

        for (int index = 0; index < listOfJsonStores.size(); index++){
            JSONObject jsonStore = (JSONObject) listOfJsonStores.get(index);
            Store store = StoreUtils.getStoreByJSON(jsonStore);
            if (store.isValid()) {
                nearListStores.add(store);
                listOfCategorys.put(store.getCategory().getId(), store.getCategory());
            }
        }

        this.homeConfiguration.setListOfCategorys(new ArrayList<Category>(listOfCategorys.values()));
        this.homeConfiguration.setListOfStores(nearListStores);

        //Define propriedades
        JSONObject proprieties = (JSONObject) stores.get("properties");
        int limitStores = Integer.parseInt(proprieties.get("limitHighlightHome").toString());
        int limitTypes = Integer.parseInt(proprieties.get("limitTypesServiceHome").toString());
        this.homeConfiguration.setLimitCategorysHome(limitTypes);
        this.homeConfiguration.setLimitStoresHome(limitStores);

    }

    public HomeConfiguration getHomeConfiguration() {
        return homeConfiguration;
    }

    public ArrayList<Store> getNearListStores() {
        return homeConfiguration != null?homeConfiguration.getListOfStores():null;
    }
}
