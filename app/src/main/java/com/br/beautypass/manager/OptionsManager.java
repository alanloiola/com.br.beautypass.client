package com.br.beautypass.manager;

import com.br.beautypass.client.R;
import com.br.beautypass.client.fragments.options.Option_fragment;

import java.util.ArrayList;
import java.util.List;

public class OptionsManager extends Manager{

    private List<Option_fragment> listOfOptions = buildListOfOptions();
    private static OptionsManager storeManager;

    public static OptionsManager getInstance(){
        if (storeManager == null){
            storeManager = new OptionsManager();
        }
        return storeManager;
    }

    private List<Option_fragment> buildListOfOptions(){
        listOfOptions = new ArrayList<Option_fragment>();

        listOfOptions.add(new Option_fragment("Notificações", R.drawable.notification));
        listOfOptions.add(new Option_fragment("Cupons", R.drawable.conectados));
        listOfOptions.add(new Option_fragment("Ajuda", R.drawable.help));

        return listOfOptions;
    }

    public List<Option_fragment> getListOfOptions(){
        return listOfOptions;
    }

}
