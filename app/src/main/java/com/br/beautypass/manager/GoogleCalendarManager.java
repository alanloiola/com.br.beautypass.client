package com.br.beautypass.manager;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.CalendarContract;

import androidx.core.app.ActivityCompat;

import com.br.beautypass.client.service.Default_Message;
import com.br.beautypass.model.GoogleCalendarEvent;

public class GoogleCalendarManager {

    public static void sendToCalendar(Default_Message window, GoogleCalendarEvent googleCalendarEvent) {
        if (ActivityCompat.checkSelfPermission(window, Manifest.permission.WRITE_CALENDAR)
                == PackageManager.PERMISSION_GRANTED) {

            Intent intent = new Intent(Intent.ACTION_INSERT);
            intent.setType("vnd.android.cursor.item/event");

            intent.putExtra(CalendarContract.Events.TITLE, googleCalendarEvent.getTitle());
            intent.putExtra(CalendarContract.Events.EVENT_LOCATION, googleCalendarEvent.getLocale());
            intent.putExtra(CalendarContract.Events.DESCRIPTION, googleCalendarEvent.getDescription());

            intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, googleCalendarEvent.getStartMillis());
            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, googleCalendarEvent.getEndMillis());
            intent.putExtra(CalendarContract.Events.EVENT_TIMEZONE, "America/Sao_Paulo");

            window.startActivity(intent);

        }else{
            window.callBackError();
        }
    }

}
