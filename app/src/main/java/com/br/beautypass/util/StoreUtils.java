package com.br.beautypass.util;

import com.br.beautypass.model.Category;
import com.br.beautypass.model.Service;
import com.br.beautypass.model.ServiceOption;
import com.br.beautypass.model.Store;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class StoreUtils {

    public static Store getStoreByJSON(JSONObject jsonStore){

        Store newStore = new Store();
        newStore.setId(Integer.parseInt(jsonStore.get("id").toString()));
        newStore.setTitle(jsonStore.get("title").toString());
        newStore.setIntroduction(jsonStore.get("introduction").toString());
        newStore.setDescription(jsonStore.get("description")==null?null:jsonStore.get("description").toString());
        newStore.setTelefone(jsonStore.get("tel").toString());
        newStore.setCnpj(jsonStore.get("cnpj").toString());
        newStore.setValid(jsonStore.get("valid").toString()=="true");
        newStore.setQtRaitings(Integer.parseInt(jsonStore.get("countRatings").toString()));
        newStore.setIdImage(Integer.parseInt(jsonStore.get("imageId").toString()));
        newStore.setScore(Float.parseFloat(jsonStore.get("score").toString()));
        newStore.setPriceLvl(Integer.parseInt(jsonStore.get("priceLvl").toString()));

        //Distancia do destino
        Object distanceFromDestiny = jsonStore.get("distance");
        if (distanceFromDestiny != null)
            newStore.setDistanceFromDestiny(Double.parseDouble(distanceFromDestiny.toString()));

        //Obtem o endereço
        JSONObject adress = (JSONObject) jsonStore.get("adress");
        newStore.setAdress(adress.get("adress").toString());
        newStore.setCep(adress.get("cep").toString());
        newStore.setComplement(adress.get("complement")==null?null:adress.get("complement").toString());

        //Coordenadas
        JSONObject coordinates = (JSONObject) jsonStore.get("coordinates");
        newStore.setCoordX(Float.parseFloat(coordinates.get("x").toString()));
        newStore.setCoordY(Float.parseFloat(coordinates.get("y").toString()));

        //Data de criação
        String createDateStr = jsonStore.get("createDate").toString();
        Date createDate = Date.valueOf(createDateStr);
        newStore.setCreateDate(createDate);

        //Categoria
        JSONObject category = (JSONObject) jsonStore.get("category");
        if (category != null ) {
            int idCategory = Integer.parseInt(category.get("id").toString());
            int idImage = Integer.parseInt(category.get("imageId").toString());
            String categoryName = category.get("title").toString();
            newStore.setCategory(idCategory, categoryName, idImage);
        }

        JSONArray services = (JSONArray) jsonStore.get("services");
        if (services != null && !services.isEmpty()){
            ArrayList<Service> listOfServices = new ArrayList<>();
            for (int indexService = 0; indexService < services.size(); indexService++){
                JSONObject serviceJSON = (JSONObject) services.get(indexService);
                listOfServices.add(getServiceByJSON(serviceJSON));
            }
            newStore.setListOfServices(listOfServices);
        }

        return newStore;
    }

    public static Service getServiceByJSON(JSONObject jsonService){
        Service service = new Service();

        service.setId(Integer.parseInt(jsonService.get("id").toString()));
        service.setTitle(jsonService.get("title").toString());
        service.setPosition(Integer.parseInt(jsonService.get("position").toString()));
        service.setValid(jsonService.get("valid").toString()=="true");
        service.setHightlight(jsonService.get("highlight").toString()=="true");
        service.setMultiOption(jsonService.get("multiOptions").toString()=="true");
        service.setDuration(Long.parseLong(jsonService.get("durationTime").toString()));

        Object description = jsonService.get("description");
        if (description != null)
            service.setDescription(description.toString());

        //Obtem o segmento
        JSONParser jsonParser = new JSONParser();
        Object segment = jsonService.get("segment");
        if (segment != null){
            try {
                JSONObject jsonSegment = (JSONObject) jsonParser.parse(segment.toString());
                int id = Integer.parseInt(jsonSegment.get("id").toString());
                String title = jsonSegment.get("title").toString();
                service.setSegment(new Category(id, title));
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        //Obtem o tipo
        Object type = jsonService.get("type");
        if (type != null){
            try {
                JSONObject jsonType = (JSONObject) jsonParser.parse(type.toString());
                int id = Integer.parseInt(jsonType.get("id").toString());
                String title = jsonType.get("title").toString();
                service.setTypeService(new Category(id, title));
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        //Obtem as opções
        Object options = jsonService.get("options");
        if (options != null){
            try {
                JSONArray arrOptions = (JSONArray) jsonParser.parse(options.toString());
                ArrayList<ServiceOption> listOfOptions = new ArrayList<>();

                for (int index = 0; index < arrOptions.size(); index++){
                    JSONObject option = (JSONObject) arrOptions.get(index);

                    ServiceOption serviceOption = new ServiceOption();
                    int id = Integer.parseInt(option.get("id").toString());
                    String title = option.get("title").toString();
                    String descriptionOp = option.get("description")==null?null:option.get("description").toString();
                    double value = Double.parseDouble(option.get("value").toString());
                    int position = Integer.parseInt(option.get("position").toString());
                    boolean valid = option.get("valid").toString().equals("true");

                    serviceOption.setId(id);
                    serviceOption.setTitle(title);
                    serviceOption.setDescription(descriptionOp);
                    serviceOption.setValid(valid);
                    serviceOption.setOrdem(position);
                    serviceOption.setValor(value);

                    //Valida se é multioption e seleciona o primeiro item
                    if (!service.isMultiOption() && index == 0){
                        serviceOption.setChecked(true);
                    }

                    if (serviceOption.isValid())
                        listOfOptions.add(serviceOption);
                }
                service.setServiceOption(listOfOptions);

            }catch(Exception e){
                e.printStackTrace();
            }
        }

        return service;
    }

    public static List<Service> getServicesFiltredBySegment(ArrayList<Service> listOfServices, Category segment){
        ArrayList<Service> listOfServicesFiltred = null;
        if (listOfServices != null && segment != null) {
            listOfServicesFiltred = new ArrayList<Service>();
            for (Service service : listOfServices) {
                if (service.getSegment() != null && service.getSegment().getId() == segment.getId())
                    listOfServicesFiltred.add(service);
            }
        }
        return listOfServicesFiltred;
    }

}
