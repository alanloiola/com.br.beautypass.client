package com.br.beautypass.util;

import androidx.annotation.DrawableRes;

import com.br.beautypass.client.R;

public class MVPUtils {

    public static @DrawableRes int getImageStoreById(int id){

        switch (id){
            case 1:
                return R.drawable.logo_store1;
            case 2:
                return R.drawable.logo_store2;
            case 3:
                return R.drawable.logo_store3;
            case 4:
                return R.drawable.logo_store4;
            case 5:
                return R.drawable.logo_store5;
            case 6:
                return R.drawable.logo_store6;
            default:
                return R.drawable.logo_store7;
        }

    }

    public static @DrawableRes int getImageTypeStoreById(int id){

        switch (id){
            case 1:
                return R.drawable.procedimento1;
            case 2:
                return R.drawable.procedimento2;
            case 3:
                return R.drawable.procedimento3;
            case 4:
                return R.drawable.procedimento4;
            default:
                return R.drawable.procedimento5;
        }

    }

}
